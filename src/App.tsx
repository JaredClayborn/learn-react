import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './Header/Header';
import Description from './Description';

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Header name="REACT"></Header>
        <Description countBy={3}></Description>
      </header>
    </div>
  );
}

export default App;
